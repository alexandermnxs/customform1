<?php
  /**
   * @file
   * Contains \Drupal\customform\Form\CustomForm
   */
namespace Drupal\customform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

 /**
  * Provides a Custom form.
  */
class CustomForm extends FormBase {
  /**
   * (@inheritodoc)
   */
  public function getFormId() {
      return 'custom_form';
  }

  /**
   * Building the Form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['text'] = [
      '#title' => t('Please fill the information below'),
      '#type' => 'label',
    ];
    $form['name'] = [
      '#title' =>  $this->t('Name'),
      '#type' => 'textfield',
      '#size' => 50,
      '#required' => TRUE,
    ];
    $form['email'] = [ 
      '#title' => $this->t('Email'),
      '#type' => 'textfield',
      '#size' => 50,
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#title' =>  $this->t('Password'),
      '#type' => 'password',
      '#size' => 50,
      '#required' => TRUE
    ];
    $form['cancel'] = array(
      '#type'   => 'submit',
      '#value'  => t('Cancel'),
      '#access' => TRUE,
      '#submit' => [
        'callback' => '::cancelForm',
      ],
    );
    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
        '#ajax' => [
          'callback' => '::submitForm',
        ],
    ];
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ];
    return $form;
  }
  /**
   * Validating the Form
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !\Drupal::service('email.validator')->isValid($value)) {
      $form_state->setErrorByName ('email', t('The email address %mail is not valid', array('%mail' => $value)));
      return;
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Submitting the Form
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(
      new HtmlCommand(
        '.result_message',
        '<div class="my_top_message">' . t('User information: ') . (($form_state->getValue('name')) . ('  ') . ($form_state->getValue('email')) . ('  ') . 
        ($form_state->getValue('password')) . ('  ') . ($form_state->getValue('nid'))) . '</div>')
    );
    return $response;
  }

  /** 
   * Cancelled from the Form
   */
  public function cancelForm(array &$form, FormStateInterface $form_state){
    $redirect = new RedirectResponse(Url::fromUserInput('/')->toString());
    $redirect->send();
  }
}